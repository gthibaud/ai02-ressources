*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Les langages à objets

## Introduction 
L’origine des **langages objet** peut être trouvée dans les réseaux sémantiques : en fixant une relation hiérarchique de base (est-un) et une relation d’appartenance, on a la structure d’un langage objet. 

*Remarque :* les autres relations se cachent dans les attributs des entités :
* Les entités hiérarchisées sont les classes (avec au sommet une classe racine souvent appelée objet)
* Les autres entités, les objets proprement dits, appartiennent aux différentes classes. 

*Types d’implémentation :* l’idée de langage à objets connaît différents types d’implémentation ayant chacune ses caractéristiques avec quelques dénominateurs communs. 

*Remarque :* les premiers objets ont été définis comme une structure de données en Lisp (les flavors). **SMALLTALK** est un des premiers langages qui a mis en oeuvre cette méthode et constitue le modèle prototypique et le plus complet des langages à objets. 

*Remarque :* des langages classiques ont ajouté cette possibilité à leur noyau habituel :
* Turbo
* Pascal 
* Cobol
* C
* Etc.  

## Principe de base : la classe 

Une **classe** est définie par :
Des **données** (variables d’instance, champs)
Des **méthodes** (procédures liées à la classe, identifiées par un sélecteur. 

*Attention :*
Pour utiliser les méthodes, il est nécessaire d’envoyer des messages aux objets de la classe. 
En principe, aucun accès direct aux données d’un objet n’est possible (encapsulation des données). 

*Exemple : *classe rectangle : 
* Variables 
    * ```(longueur (valeur : un nombre))```
    * ```(largeur (valeur : un nombre))```
    * ```(coin (valeur : un point))```
* Méthodes 
    * ```(aire (longueur * largeur))```
    * ```(display (draw-rect (coin (longueur largeur)))```
    * ```(init (a b p) (longueur <- a)(largeur <- b)(coin <- p))```

*Exemple :* classe point :
* Variables 
    * ```(cx (valeur : un nombre))```
    * ```(cy (valeur : un nombre))```
* Méthode 
    * ```(display (plot cx cy))```
    * ```(init (a b) (cy <- a)(cy <- b))```

*Exemple :* définition des objets rec-1 et pointA, instances des classes rectangle et point : 
* ```(new rectangle rec-1)```
* ```(new point pointA)```

*Exemple :* envoi de messages :
* ```(send pointA init 25 10)``` : ce message initialize les valorous de pointA
* ```(send rec-1 init 2510 pointA)``` : ce message initialise les valeurs de rec-1
* ```(send aire rec-1)``` : ce message permet d’obtenir la valeur de l’aire de rec-1 : 250

**Polymorphisme :** la méthode init est différente selon l’objet auquel elle s’applique. 

**Héritage :** une classe peut être définie par rapport à une classe déjà existante. 

*Exemple :* classe rectangle-plein :
* Variables
    * ```(couleur (possibilités : jaune bleu rouge))```
* Méthodes
    * ```(display (super display) (fill couleur))```
    * ```(init (a b c p)(super init a b p)(couleur <- c))```

*Remarque :* celle nouvelle classe possède les propriétés de la classe rectangle.

## Synthèse 

**Classe :** un objet est déterminé par différents attributs définis dans sa classe. Chaque attribut peut être type. 

**Héritage :** avec la possibilité de définir des classes « emboîtées », on utilise de façon fondamentale les mécanismes d’héritage. 

**Méthodes :** les procédures associées aux classes spécifiant le comportement de ces dernières : ce sont les méthodes. 

**Message :** les objets communiquent par l’envoi de messages qui invoquent les méthodes à mettre en oeuvre. 