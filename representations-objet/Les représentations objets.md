*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Les représentations objets

## Introduction

### 1. Préambule 

La perception et la modélisation du monde réel est un problème philosophique central qui n’a toujours pas de réponse. 

*Rappel :* L’objectif de l’IA est de représenter ce monde réel de façon suffisamment fidèle, pour que des programmes puissent agir sur cette représentation et arriver à des conclusions semblables à celle d’un opérateur humain. 

*Remarque :* Dans cette représentation, la notion d’objet semble essentielle :
* Un monde est peuplé d’objets réels ou abstraits. 
* Ces objets sont perçus par nos sens et d’une certaine façon qualifiés. 

*Remarque :* Les objets ont donc des qualités (propriétés et attributs). Cela justifie l’approche linguistique qui définit les termes en fonction les uns des autres. 

### 2. Lacunes des réseaux sémantiques 

Virtuellement, tout peut être représenté par des noeuds et des liens, notamment : 
* Des faits
* Des pointeurs
* Le sens de phrases
* Des propositions
* Des actions
* Des événements 
* Des propriétés 
* Des prédicats
* Etc. 

*Attention :* Ces différents niveaux de représentation sont mélangés, donc confusion de notation et difficultés d’expliquer le fonctionnement des interpréteurs. 

### 3. Solutions

Du fait des lacunes des réseaux sémantiques différents travaux ont vu le jour : 
* **Les frames :** granules de connaissances plus importantes que les noeuds d’un réseau sémantique. 
* **Les logiques de description :** modèle à mi-chemin entre les réseaux sémantiques et les Frames, dont le premier représentant s’appelle KL-ONE.
* **Les graphes conceptuels :** modèle basé sur la logique. 

## Points communs et différences des solutions

### 1. Réification des objets du monde réel

**Objet :** Un objet est défini par un ensemble fini de propriétés (plus formellement des prédicats n-aires) qui peuvent être : 
* Des couples attributs / valeurs,
* Des relations entre objets. 

*Remarque :* Un attribut ou une relation donnée peut posséder plusieurs valeurs. 

Il existe généralement deux catégories d’objets :
* Les objets **individuels**, ou individus, ou instances (ex : Médor le chien, Félix le chat)
* Les objets **abstraits**, ou classes (ex : chien, chat). 

### 2. Structuration en hiérarchie

Les objets sont structurés dans une hiérarchie, encore appelée **graphe d’héritage** par des liens : 
* **is-a** ou kind-of : relation de généralisation / spécialisation entre les objets abstraits. 
* **member-of** : relation entre objets individuels et objets abstraits. 

*Méthode :* 
Soit G = (X,H) le graphe d’héritage avec : 
* X l’ensemble des **objets**
* H l’ensemble des **arcs**

X = Y U Z avec :
* Y l’ensemble des **classes**
* Z l’ensemble des **instances**

H = H1 U H2 avec : 
* H1 l’ensemble des liens **is-a** entre classes
* H2 l’ensemble des liens **member-of**

≤ H est de l’ordre induit par le graphe G avec :
* Quelques soient x et y appartenant à X tels que si x ≤ H y alors y est un ancêtre de x et x est un descendant de y.  

*Remarque :* le graphe G possède toujours une racine unique. 

**Mécanisme de déduction :**
* À un niveau donné de la hiérarchie, un objet ne possède - déclaré dans l’objet considéré - que les propriétés qui lui sont spécifiques et qui ne sont, en général, pas présentes chez ses ancêtres. 
* Toute propriété non présente dans un objet est recherchée chez ses ancêtres par un parcours de sa hiérarchie. 

*Remarque :* Le graphe d’héritage peut être : 
* Un arbre, l’héritage est simple dans ce cas. 
* Un graphe orienté sans circuit - ou un treillis -, l’héritage est multiple dans ce cas : plusieurs ancêtres ou objets plus abstraits, pour un objet donné. 

Attention, en cas d’héritage multiple, il peut y avoir des conflits d’héritage (deux propriétés (couple attribut valeur) de même nom (même attribut) sont véritables et possèdent des valeurs différentes. 

### 3. Définition en extension / intension

Dans les représentation objet, les objets sont définis :
* En **extension :** 
    * L’extension propre d’une classe est définie par un ensemble d’individus. 
    * L’extension au sens large d’une classe est définie par un ensemble d’instances et de classes plus spécifiques. 
* En **intension (ou compréhension) :**
    * Un objet est défini en intension par l’ensemble des propriétés qu’il possède ou hérite. 

*Remarque :* Pour un objet abstrait, il s’agit dans certains cas des propriétés communes à tous ses membres (ceux appartenant à son extension). 

**Rapports entre extensions :** Soient Extp(x) l’extension propre de l’objet x et Ext(x) l’extension au sens large de x, alors :
* Quelques soient x et y appartenant à Y tels que x ≤ H, y => Ext(x) est inclus dans Ext(y).
* Quelques soient y appartenant à Y et z appartenant à Z tels que z ≤ H, y => z appartient à Extp(x).

**Rapports entre intensions :** Soient z un objet, Int(z) l’intension de z ou ensemble de propriétés définissant z, c’est-à-dire celles déclarées dans z et celles héritées de ses ancêtres, alors : 
* Quelques soient x et y appartenant à X, tels que x ≤ H, y => Int(y) est inclus dans Int(x).

**Raisonnement monotone :** Un objet possède toutes les propriétés déclarées chez ses ancêtres. 

**Raisonnement non monotone :** Un objet ne possède pas toutes les propriétés déclarées chez ses ancêtres. 

*Remarque :* Le modèle de l’approche des **Frames** de Minsky et celui de l’approche des concepts ou objets conceptuels structurés de Brachman et Schmolze peuvent se comparer grâce aux rapports qu’entretiennent l’intension et l’extension.

### 4. Modèle Objet Conceptuel Structuré
*Remarque :* Le modèle classe / instance ou d’objet conceptuel structuré est conforme au modèle classique de la catégorisation ou modèle des **CNS **(conditions nécessaires et suffisantes). 

*Définition :*
* Les catégories sont constituées sur la base de catégories communes. 
* Chaque propriété prise unitairement est nécessaire et elles sont globalement suffisantes pour décider de l’appartenance à la catégorie. 
* Un exemplaire appartient à la catégorie si et seulement si il possède l’ensemble des propriétés de la catégorie. 
* Organisation intra-catégorielle : tous les exemplaires d’une catégorie ont un statut identique. 
* Organisation inter-catégorielle : les catégories sont organisées dans une taxinomie par une relation d’inclusion stricte. 

### 5. Théorie du prototype
*Remarque :* Les expériences en psychologie cognitive sur les phénomènes de catégorisation ont mis à jour une structure intra-catégorielle, non plus basée sur l’équivalence des exemplaires de la catégorie, mais sur une distribution de ceux-ci selon un gradient de typicalité. 

*Méthode :* 
* Les exemplaires se distribuent des plus représentatifs aux moins représentatifs. 
* La non-équivalence du statut des exemplaires dans une catégorie est un fait empirique relatif aux jugements que les gens émettent sur l’appartenance à une catégorie. 

**Théorie du prototype :** Certains membres de la catégorie, appelés atypiques, ne partagent pas avec le prototype toutes ses propriétés. 
Dans le cas du modèle prototype, il n’y a plus d’équivalence entre extension et intension. 

### 6. Mécanismes d’inférence 

Les principaux mécanismes d’inférence sont l’héritage, le filtrage et la classification : 
* **L’héritage :** mécanisme de partage de propriétés entre des entités structurées dans une hiérarchie qui induit un raisonnement monotone ou non monotone selon les systèmes. 
* **Le filtrage :** recherche d’un ensemble d’objets qui satisfont à certains critères donnés.  Remarque : le filtrage est souvent basé sur une une logique à trois valeurs : vrai, faux et inconnu (monde ouvert). 
* **La classification :** opération qui permet de placer un objet x dans un graphe d’héritage. (trouver les ancêtres les plus spécifiques qui deviendront les ancêtres immédiats). 

*Méthode :* Deux cas sont à considérer :
* **Une instance x :** il s’agit de trouver tous les objets A, ancêtres de x, qui sont les plus spécifiques. Ces derniers deviendront donc les ancêtres immédiats de x (reliés à x par un seul lien). 
* **Une classe x :** il s’agit de trouver tous les objets A, ancêtres de x, qui sont les plus généraux. Les objets A seront les ancêtres immédiats de x et les objets B les descendants immédiats de x. 

##Synthèse

Frames :
* Prototype
* Héritage multiple avec conflits + filtrage + réflexes 

Logique de description :
* CNS
* Héritage multiple sans conflit
* Classification (subsomption)

Graphes conceptuels :
* CNS
* Héritage multiple sans conflit 
* Objet = sous-graphe 
* Objets organisés dans un treillis
* Classification par appariement de graphes. 