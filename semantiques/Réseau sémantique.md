*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Réseaux sémantiques

## Un peu d'histoire

C'est Quillian qui a proposé le premier en 1961 puis en 1968 de construire un modèle de la « mémoire humaine » fondé sur un réseau sémantique de mots construits à partir d'expériences en psycholinguistique où des humains sont soumis à des tâches terminologiques portant sur le sens des mots et où la mesure physique de leurs temps de réponse permet de définir des « distances sémantiques ». 

*Remarque :* Ces recherches sont à la base de l'école sémantique lexicale qui a produit le réseau sémantique **Wordnet** très utilisé aujourd'hui en traitement de la langue. 

## Définition

Les **réseaux sémantiques** sont : 
* Un mécanisme général d'association pour représenter le sens des mots. 
* Un ensemble de nœuds et d'associations représentées par des liens. 
* Un graphe orienté acyclique dont les noeuds et les arcs sont étiquetés. 
* Un graphe d'héritage, structuré au moyen d'une relation de généralisation/spécialisation qui relie entre eux des objets « sémantiquement proches ». 

*Exemple :*

<img style="display: block;margin: auto;width:50%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-1.png">

## Problèmes

Bien que très intéressants, les réseaux sémantiques présentent quelques problèmes de représentation. 

### 1. Liens assertionnels

Un lien assertionnel peut porter sur :
* Un individu et un ensemble : « Marie est une femme »

<img style="display: block;margin: auto;width:90%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-2.png">
* Deux ensembles : « Une femme est un humain »

<img style="display: block;margin: auto;width:90%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-3.png">

*Attention :*
* Les réseaux sémantiques ne font pas la distinction et emploient souvent la même relation (is-a) pour représenter deux notions sémantiquement très différentes. 
* Ceci a aussi conduit à faire une confusion entre les propriétés des éléments d'un ensemble et les propriétés de l'ensemble en tant que tel. 


### 2.Liens structurels
Les liens structurels portent sur des relations entre ensembles. Voici quelques problèmes liés à leur représentation. 
* **Héritage de propriétés** (is-a) : 
    * Exemple : FEMME is-a HUMAIN
* **Composition** :
    * Exemple : HUMAIN = { tête, corps, membres }
* **Domaine** : ensemble de valeurs possibles :
    * Exemple : Marie : age[0..100] doit être différencié des valeurs effectives Marie->age(20) 
* **Possession** :
    * Exemple : 
        * Droit légal : Marie a un passé.
        * Service : Marie a une ligne ADSL.
        * Composition : un vélo a deux roues et un guidon.
        * Intrasèque : Marie a un don musical. 

### 3. Manques
De plus, certains liens manquent dans les réseaux sémantiques :
* **Négation :**
    * Exemple : Marie n’est pas mariée. 
* **Disjonction :**
    * Exemple : Dominique est soit un homme soit une femme. 
* **Quantification universelle :**
    * Exemple : Tous les enfants de Marie sont des filles
* etc. 

## NETL

C'est Fahlman qui a popularisé les réseaux sémantiques en proposant une approche de représentation et d'utilisation des connaissances du monde réel pour un traitement automatique en machine : NETL 
* Nœuds et arcs + mécanisme de marquage. 
* Tout nœud ou arc est accessible.
* Les marques se propagent en parallèle. 

### 1. Noeuds et arcs
* Noeud :

<img style="display: block;margin: auto;width:40%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-4.png">

* Arc : 

<img style="display: block;margin: auto;width:20%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-5.png">

### 2. Négation

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-6.png">

### 3. Propagation

<img style="display: block;margin: auto;width:70%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-7.png">

*Exemple :* Existe-t-il des personnes de sang royal possédant une arme ?

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-8.png">

* Étape 1 : on marque « personne-de-sang-royal » avec M1, arme avec M2.

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-9.png">

* Étape 2 : On propage le long des arcs is-a vers le bas.

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-10.png">

* Étape 3 : On envoie un message aux arcs possède en leur demandant de retourner une réponse s'ils ont une marque M1 à gauche et une marque M2 à droite. 

### 4. Détection d'incohérences

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-11.png">

*Exemple :* Clyde est marqué, les marques sont propagées vers le haut : si un arc split est marqué des deux côtés, alors la modification proposée est incohérente.

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-12.png">

### 5. Notion de rôle

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-13.png">

*Exemple :* "La mère de Clyde est Bertha"

<img style="display: block;margin: auto;width:50%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-14.png">

*Exemple :* "La patte avant gauche de Clyde"

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-15.png">

*Exemple :* "Les pattes de Clyde"

<img style="display: block;margin: auto;width:50%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-16.png">

*Exemple :* Cascade de rôles"

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-17.png">

### 6. Caractéristiques des objets

*Exemple :*

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-18.png">

### 7. Notion d'univers

*Exemple :*

<img style="display: block;margin: auto;width:40%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/semantiques/semantique-19.png">

## Synthèse

Pour pouvoir représenter l'ensemble des connaissances sur le monde réel de nombreux autres concepts sont nécessaires. 

Les réseaux sémantiques ont été proposés et développés par Fahlman dans son travail de thèse au MIT. 
Il a montré que la plupart des connaissances sur le monde réel pouvaient être représentées et restaient compatibles avec le mécanisme de propagation de marques en parallèle. 

*Attention :* Nécessité d'une machine parallèle pouvant permettre de réaliser ce mécanisme. 

*Épilogue :* Hillis, travaillant au MIT, a réalisé quelques années plus tard la Connection Machine. L'utilisation de cette machine a montré le bien fondé de l'approche de Fahlman. 
Entre temps, la notion de frame a été développée (macro-noeuds). De plus, le prix prohibitif de la Connection Machine n'a pas favorisé l'utilisation des réseaux sémantiques.