*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Représentation des connaissances

### 1. Objectifs

**Représentation des connaissances :** support préalable à toute manipulation de l’information. On peut organiser, classer, chercher, extraire, déduire, établir des contradiction, contradictions réviser etc. 

### 2. Formalismes et représentation

On va parler de représentation de connaissances **explicites**, on choisit un formalisme de représentation pour permettre la manipulation des informations. 

Le choix du formalisme de représentation est très important, il dépend du domaine d’application, des opérations qu’on souhaite faire sur le formalisme et de la culture du modélisateur. 

<img style="display: block;margin: auto;width:80%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/representation-des-connaissances/representation-des-connaissances-1.png">

* **Les approches non logiques :** 
    * Le sens intrinsèque attribué aux structures (objet en soi)
    * L’existence de procédures adaptées et souvent efficaces
    * Une plus grande facilité de révision. 
* **Les approches logiques :**
    * Procédure génériques (plus ou moins adaptées selon le choix de la logique)
    * Possibilité de caractériser des propriétés du formalisme et des procédures (expressivité, complétude etc). 