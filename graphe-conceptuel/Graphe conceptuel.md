*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Graphes conceptuels

## Introduction 

La théorie des **graphes conceptuels**, développé par SOWA (1984), est un formalisme général de représentation des connaissances fondé sur la logique. Ce langage est basé sur : 
* La linguistique 
* La psychologie
* La philosophie 

Son objectif est de développer un système de logique qui représente de façon simple et pratique les structures du langage naturel. 

## Définition formelle et notations 

Après avoir précisé la notation formelles des **graphes conceptuels**, nous en donnerons deux façon de les noter. 

### 1. Définition formelle 

*Définition :*
* Les graphes conceptuels sont des graphes **finis**, **connexes** et **bipartites**. 
* Les deux sortes de noeuds d’un graphe bipartite sont les **concepts** et les **relations conceptuelles**. 
* Chaque relation conceptuelle possède un **arc** ou plus, chacun d’entre eux devant être relié à un concept. 

*Remarque :*
* Si on a n arcs, la relation est dire **n-adique**.
* Un unique concept peut former un graphe conceptuel, mais chaque arc de chaque relation conceptuelle doit être lié à un concept quelconque.

### 2. Deux notations 

**Notation graphique :**
* Concepts : rectangles
* Relations : ovales
* Arcs : flèches

*Exemple :*
<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/graphe-conceptuel/graphe-conceptuel-1.png">

**Notation linéaire :**
* Concepts : entre crochets 
* Relations : entre parenthèses 
* Arcs : flèches 

*Exemple : *
```
[ALLER] -
    (AGNT) -> [PERSONNE:Pierre]
    (DEST) -> [VILLE:Montréal]
    (INST) -> [VOITURE]
```

## Positionnement 

Précisons les liens qui existent entre les GC, les frames et la logique. 

### 1. GC et Frames

Un GC ressemble à un Frame : 
* Les relations conceptuelles s’apparentent aux « slots »
* Les étiquettes de type s’apparentent aux contraintes
* Les noms Pierre et Montréal s’apparentent aux contenus des « slots »

*Remarque :* contrairement aux Frames qui ne savent représenter qu’une sous section de la logique, les GC sont plus généraux et permettent de représenter toute la logique. 

### 2. GC et logique

Les GC reposent sur un système de logique et peuvent ainsi être représentés par la logique des prédicats. 

*Exemple :* l’analyse de l’exemple précédent révèle :
* Prédicats : personne(x), aller(x), ville(x), et voiture(x)
* Les relations conceptuelles : agnt(x,y), dest(x,y), int(x,y) 

*Exemple :* l’expression de ce graphe en logique des prédicats du premier ordre donne donc l’expression suivante :

<img style="display: block;margin: auto;width:60%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/graphe-conceptuel/graphe-conceptuel-2.png">

## Mise en oeuvre 

Illustrons la représentation des concepts, relations conceptuelles, les graphes canoniques et certaines opérations. 

## 1. Les concepts 

Définition : un concept est un couple : ```(type, référent)```

*Exemple :* 
* Concept générique : ```[personne]``` ou ```[personne : *]```
* Concept individuel : ```[personne : #1]```
* Nom propre : ```[personne : Jean]```
* Ensemble générique : ```[personnes : {*}]```
* Ensemble de cardinal spécifié : ```[personne : {*}@3]```
* Mesure associés à une unité : ```[hauteur : @3m]```
* Ensemble défini en extension : ```[personne : {Jean, Marc, Arthur}]```

## 2. Les relations conceptuelles 

**Agent :** ```(agnt) relie [action] à [animé]``` : le concept animé représente l’acteur de l’action

**Expérienceur :** ```(expr) relie [état] à [animé]``` : le concept animé ressent cet état (Marie a chaud)

**Instrument :** ```(inst) relie [entité] à [action]``` : entité est impliquée de manière causale

**Objet :** ```(obj) relie [action] à [entité]``` sur laquelle porte l’action : le chien mange un os (os = objet)

### 3. Les graphes canoniques 

Les **graphes canoniques** définissent des conditions d’emploi des concepts ou des possibilités de combinaisons de concepts. 

*Remarque :* les autres graphes seront dérivés à partir des graphes canoniques. 
*Exemple :* interne
```
[interne] -
    (agnt) <- [pratique] -
        (obj) -> [medecine]
        (lieu) -> [hopital]
```
*Exemple :* juger
```
[interne] -
    (agnt) -> [personne]
    (obj) -> [proposition]
```
*Exemple : les internes jugent une proposition importante
```
[interne] -
    (agnt) -> [interne] -
    (obj) -> [proposition]
        (attr) -> [important]
```

### 4. Les opérations 

Les opérations sur les graphes permettent de manipuler les GC lors d’une analyse sémantique ou d’exprimer des phrases plus ou moins concises lors d’une génération. 

*Exemple : *
* Copie
* Restrictions
* Simplification
* Jointure
    * Jointure maximale
    * Jointure dirigée 