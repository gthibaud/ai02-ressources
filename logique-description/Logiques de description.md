*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Les logiques de description

## Introduction

Les **logiques de description** (Brachman et Schmolze, 1977), offrent un modèle à mi-chemin entre les réseaux sémantiques et les Frames de Minsky. Le premier représentant s’appelle **KL-ONE**.

## Le langage KL-ONE

Le langage **KL-ONE** a été créé par Brachman en 1985. Il s’agit d’un langage de représentation dont la sémantique est bien fondée, c’est-à-dire externe à la représentation et aux algorithmes qui opèrent sur celle-ci. À l’origine, KL-ONE était utilisé dans les domaines d’application suivants : compréhension de la langue naturelle, génération automatique de langue naturelle, etc.

*Définition :* KL-ONE est un langage de représentation des connaissances à la frontière des réseaux sémantiques et des Frames :
Langage de représentation de concepts proches de la notion de **sémantique lexicale**
Les concepts de KL-ONE sont munis d’attributs appelés **rôle**
Des contraintes sur le type et la cardinalice de sa valeur sont associés à tout rôle, à la manière des facettes d’un langage de Frames.

*Remarque :* KL-ONE est basé sur la subsomption de termes. C’est un système à héritage structuré. 

KL-ONE est à l’origine de nombreux autres langages : BACK, CANDIDE, CLASSIC, DRL, KANDOR, KL-CONC, KL-MAGMA, KL-TWO, KNET, KREM, K-REP, KRYPTON, L-LILOG, LOOM, MANTRA, MESON, NIKL, SB-ONE, SPHINX, DAML+OIL, OWL. 

## Caractéristiques des logiques de description 

Les principales caractéristiques des logiques de description sont une représentation bipartite et l’usage de primitives épistémologiques. 

### 1. Représentation bipartite 

*Définition :*
* Partie descriptive appelée **Tbox** pour « Terminological box » 
* Partie assertionnelle appelée **Abox** pour « Assertional box »
*Remarque :* KL-ONE n’est pas bipartite, c’est un langage de description de concepts, sous la forme de prédicats, sans assertions sur le monde. 

*Raison d’être de Krypton :* Composant terminologique et assertionnel fondé sur la logique des prédicats :
* Les opérations sur les connaissances et les « faits » ne sont pas les mêmes. 
* Induit des problèmes : liaison entre les deux composants. 

### 2. Primitives épistémologiques

Une logique de description fournit des primitives épistémologiques (primitives de représentation) :
* Des concepts primitifs ou définis,
* Des rôles,
* Des opérations (and, or, not, some, all, atleast, atmost… sur ces éléments primitifs du langage,
* Un mécanisme de classification fondé sur la relation de subsomption entre concepts ou rôles. 
-> Cela induit généralement un graphe orienté sans circuit entre les concepts et entre les rôles. 

Les concepts peuvent être de deux types : 
* **Concept primitif :** définition incomplète d’un concept (Conditions Nécessaires mais non suffisantes). 
    * Exemple : Définition du concept primitif polygone : Polygone peut être défini comme une « figure géométrique avec N côtés qui sont des segments de droite » -> Nous avons des CN mais non suffisantes pour définir qu’un individu appartient à cette classe : toute figure non fermée satisfait ces conditions.  
* **Concept défini :** définition complète d’un concept (CNS). 
    * *Exemple :* définition du concept défini triangle : Un triangle peut être un concept défini, dérivé de polygone : « un triangle est un polygone à exactement trois côtés ».

*Remarque :* la classification ne peut opérer que sur des concepts définis. 

*Exemple :* Définition 1 du concept moto :
```
(Cprim Véhicule
    (and
        (atleast 1 Roue)
        (atleast 1 Moteur)))
(Cdef Moto
    (and
        (atleast 2 Roue)
        (atleast 1 Moteur)))
Moto est un Véhicule
```
*Exemple :* Définition 2 du concept moto :
```
(Cprim Véhicule
    (and
        (atleast 1 Roue)
        (atleast 1 Moteur)))
(Cdef Moto
    (and
        Véhicule
        (atleast 2 Roue)))
Moto est un Véhicule ?
```
## Mise en oeuvre dans KL-ONE

Illustrons la définition des concepts et des rôles avec KL-ONE. 

### 1. Les concepts 

Les concepts sont organisés sous la forme d’une taxinomie, éventuellement multiple, utilisant le lien de subsomption de façon à permettre l’héritage de leurs propriétés. 

Il existe deux types de concepts : 
* **Concept générique** : extension = plusieurs individus. 
* **Concept individuel** : extension = un individu (exemple : le roi de France, étoile du soir, étoile du matin). 

Taxinomie : Deux liens particuliers permettent d’organiser la taxinomie de ce concept :
* Lien **« sorte-de »** : permet de relier des concepts génériques. 
* Lien **« est-un » **: correspond à la relation d’instanciation entre un concept générique et un concept individuel. 

### 2. Les rôles 

*Rôle :*
* Permet de mettre en relation des concepts
* Permet d’exprimer les conditions de la définition d’un concept
* Peut être générique ou individuel

*Attention : *
* Les rôles génériques une relient que des concepts génériques. 
* Les rôles individuels permettent de relier des concepts individuels. 

*Remarque :* Tous les rôles peuvent être munis de propriétés :
* Restrictions de valeur 
* Restriction de nombre
* Différentiation
* Description structurelle

*Rôle générique :*
* Un rôle générique est décrit ou spécifié par une restriction de valeur et possède une restriction de nombre ou cardinalité [n, m]
* Les restrictions de valeurs sont de type nécessaire

*Exemple :*
<img style="display: block;margin: auto;width:80%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/logique-description/logique-description-1.png">

**Restriction de rôle :** joue un rôle similaire à la relation de subsomption, mais se rapporte aux rôles génériques. 

*Exemple :*
<img style="display: block;margin: auto;width:80%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/logique-description/logique-description-2.png">

**Différentiation de rôles :** un rôle générique différencie un autre si il dénote une sous-relation de la relation dénotée par l’autre. 
*Exemple : *

<img style="display: block;margin: auto;width:80%" src="https://gitlab.utc.fr/gthibaud/ai02-ressources/raw/master/logique-description/logique-description-3.png">

### 3. Synthèse 

KL-ONE est un langage de description de concepts sous la forme de prédicats. 
Il ne comporte que des connaissances terminologiques : Triangle = polygone à trois côtés. 

Brachman préconise l’utilisation de **connaissances assertionnelles**, ce qu’il applique dans le système KRYPTON :
* **TBox :** logique de prédicats, c’est la terminologie qui décrit les propriétés du monde en intension. 
* **ABox :** proche de la logique du premier ordre, c’est l’ensemble des connaissances concernant les individus peuplant effectivement le monde. 