*Note : pour afficher ce document, ajoutez une extension pour votre navigateur, ou bien utilisez un logiciel capable d'afficher du Markdown.*

# Les frames

En 1975, Minsky propose une approche basée sur la notion de **frame**.

**But :** Résoudre un certain nombre de problèmes en intelligence artificielle, notamment la vision par ordinateur, l’analyse du discours et la compréhension du langage. 

**Principe :** Face à une nouvelle situation, on sélectionne en mémoire une structure de données appelée frame qui est la plus proche possible de la situation courante. Cette structure doit subir quelques modifications pour s’adapter à la réalité puis est mémorisée dans le système de frames et ainsi reconnaître ou identifier certains objets. 

## Définition 

Un frame est une structure de données qui représente une situation caractéristique ou prototypique. Une structure de frame se compose de plusieurs éléments, appelés slots ou éléments terminaux (sur différents niveaux). 

*Remarque : *
* Un frame est une sorte de réseau de relations et de noeuds : 
    * Chaque slot peut être lui-même un frame. 
    * Chaque slot peut prendre une valeur par défaut qui est remplacée afin de mieux cadrer avec la réalité et servir de variable. 

Le frame est une structure à trois niveaux emboîtés : le triplet frame, slot, facette :
```
(Frames
    (slot (facette_value)
          (facette_value) …)
    (slot (facette_value)
          (facette_value) …) …)
```
Un **slot** décrit les différentes propriétés du frame. 

Une **facette** est toujours modalité descriptive ou comportementale d’un slot. À une facette est toujours associée une valeur. 

Il existe plusieurs types de facettes : 
* Les facettes **déclaratives :** 
    * Facette de typage (ex . ```$un```, ```$liste-de```, ```$sauf```, ```$intervalle```, ```$a-verifier```…)
    * Facette de valeurs (```$valeur```, ```$défaut```…)
* Les facettes **procédurales :** elles permettent d’exécuter une procédure, un bout de code dans une situation définie (ex : $si-besoin, $si-possible, $si-ajout, $si-enlève…) 

*Exemple :* Frame du concept date
```
(date
    sorte-de $valeur objet
    jour     $un entier
             $intervalle [1 31]
    mois     $un chaîne
             $domaine "janvier" "février" "mars" "avril" "mai" "juin" "juillet" "aout" "septembre" "octobre" "novembre" "décembre"
    année    $un entier)
```
*Exemple :* Frame d'une date 
```
(date-23
    est-un $valeur date
    jour   $valeur 15
    mois   $valeur « avril »
    année  $valeur 1978)
```
*Exemple :* Frame du concept de division euclidienne 
```
(division-euclidienne
    sorte-de  $valeur méthode
    dividende $un entier
    diviseur  $un entier
              $sauf 0
    reste     $un entier)
```

## Exploitation

Lors de la lecture d’un slot, la valeur peut être obtenue par la facette ```$valeur```, ```$défaut``` ou ```$si-besoin```. 
### 1. La lecture

La lecture d’un slot peut entraîner le parcours de la hiérarchie considérée. Trois stratégies principales peuvent être mises en oeuvre :
* La **stratégie en I :** Seules les facettes ```$valeur``` sont consultées dans la hiérarchie de l’objet. 
* La **stratégie en Z :** Les facettes ```$valeur```, ```$défaut``` et ```$si-besoin``` sont consultées dans cet ordre à chaque niveau de hiérarchie. 
* La **stratégie en N :** Les facettes ```$valeur``` sont consultées sur toute la hiérarchie (du bas vers le haut), puis on fait de même avec les facettes ```$défaut``` puis ```$si-besoin```. 

### 2. L’écriture

*Méthode :*
* Vérifications a priori dans l’ordre descendant de la hiérarchie (examen des facettes de typage et déclenchement des réflexes ```$si-possible```). 
* Mise en place de la valeur et enfin propagation des modifications, déclenchement des réflexes ```$si-ajout``` dans l’ordre descendant de la hiérarchie. 

### 3. La suppression 

*Méthode :* Il y a d’abord l’effacement de la valeur puis propagation des réflexes ```$si-enlève``` dans l’ordre ascendant de la hiérarchie. 

## Différents modèles de frames

On peut scinder la famille des frames en deux sous-familles : 
* Une sous famille dérivée du modèle **classes / instances**
* Une sous famille inspirée de la **théorie du prototype**

*Exemple :* Modèle classes / instances : 
```
(meuble 
    sorte-de    $valeur objet)
(table
    sorte-de    $valeur meuble
    forme       $un chaîne
                $domaine « ronde » « ovale » « rectangulaire »
    nbr-pieds   $un entier
                $intervalle [1 15]
    composé-de  $liste-de $un plateau $liste-de pieds)
(plateau 
    sorte-de    $valeur objet)
(pieds 
    sorte-de    $valeur objet)
```
*Remarque :* Un slot muni d’un domaine de valeurs est équivalent à une disjonction de couples (attribut / valeur) - ou de propriétés. 

*Exemple :* Modèle protoype
```
(pieds 
    sorte-de    $valeur objet)
(meuble 
    sorte-de    $valeur objet)
(table 
    sorte-de    $valeur meuble
    forme       $un chaîne
                $défaut « rectangulaire »
    nbr-pieds   $un entier
                $défaut 4
    composé-de  $liste-de $un plateau $liste-de pieds)
(plateau 
    sorte-de    $valeur objet)
```
Un sous objet d’une table n’aura pas obligatoirement les mêmes propriétés puisque les valeurs des attributs sont par défaut. 